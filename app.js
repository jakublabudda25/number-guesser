let min = 1,
	max = 10,
	winningNum = getRandomNum(min, max),
	guessesLeft = 3;

const gameWrapper = document.querySelector('#game'),
	  minNum = document.querySelector('.min-num'),
	  maxNum = document.querySelector('.max-num'),
	  guessBtn = document.querySelector('#guess-btn'),
	  guessInput = document.querySelector('#guess-input'),
	  message = document.querySelector('.message');

minNum.textContent = min;
maxNum.textContent = max;

gameWrapper.addEventListener('mousedown', function(e) {
	console.log('won');
	if(e.target.className === 'play-again') {
		window.location.reload();
	}
});


guessBtn.addEventListener('click', function(e) {
	let guess = parseInt(guessInput.value);

	if(isNaN(guess) || guess < min || guess > max) {
		setMessage(`Please enter a number between ${min} and ${max}`, 'red');
		return false;
	} 
	
	if(guess === winningNum) {
		gameOver(true, `You won, winning number is ${winningNum}`);
	} else {
		guessesLeft -= 1;

		if(guessesLeft === 0) {
			gameOver(false, `You lost, winning number is ${winningNum}`);
		} else {
			setMessage(`Answer wrong. You have still ${guessesLeft} chances`, 'red');
			guessInput.style.borderColor = 'red';
			guessInput.value = '';
		}
	}
});

function gameOver(won, msg) {
	let color;
	won === true ? color = 'green' : color = 'red';
	setMessage(msg);
	
	guessInput.disabled = true; 
	guessInput.style.borderColor = color;
	message.style.color = color;

	guessBtn.value = 'Play Again';
	guessBtn.className += 'play-again';
}

function getRandomNum() {
	return Math.floor(Math.random()*(max-min+1)+min); 
}

function setMessage(msg, color) {
	message.style.color = color;
	message.textContent = msg;
}